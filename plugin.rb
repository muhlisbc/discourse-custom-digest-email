# name: discourse-custom-digest-email
# about: Custom discourse digest email
# authors: Muhlis Budi Cahyono (muhlisbc@gmail.com)
# version: 0.1
# url: http://git.dev.abylina.com/momon/discourse-custom-digest-email

after_initialize {

  module ::CustomDigest
    def self.included(base)
      base.class_eval do
        prepend_view_path File.expand_path("../views", __FILE__)

        def digest(user, opts={})
          @since = opts[:since] || user.last_emailed_at || 1.day.ago
          @since_formatted = short_date(@since)

          topics = Topic
            .joins(:posts)
            .includes(:posts)
            .for_digest(user, 100.years.ago)
            .where("posts.created_at > ?", @since)

          unless user.staff?
            topics = topics.where("posts.post_type <> ?", Post.types[:whisper])
          end

          @new_topics = topics.where("topics.created_at > ?", @since).uniq
          @existing_topics = topics.where("topics.created_at <= ?", @since).uniq
          @topics = topics.uniq

          return if @topics.empty?

          build_summary_for(user)
          opts = {
            from_alias: I18n.t('user_notifications.mailing_list.from', site_name: SiteSetting.title),
            subject: I18n.t('user_notifications.mailing_list.subject_template', email_prefix: @email_prefix, date: @date),
            add_unsubscribe_link: true,
            unsubscribe_url: "#{Discourse.base_url}/email/unsubscribe/#{@unsubscribe_key}",
          }

          apply_notification_styles(build_email(@user.email, opts))
        end
      end
    end
  end

  module ::CustomDigestJob
    def self.included(base)
      base.class_eval do
        def target_user_ids
          query = User.real
                      .not_suspended
                      .activated
                      .where(staged: false)
                      .joins(:user_option, :user_stat)
                      .where("user_options.email_digests")
                      .where("user_stats.bounce_score < #{SiteSetting.bounce_score_threshold}")
                      .where("COALESCE(last_emailed_at, '2010-01-01') <= CURRENT_TIMESTAMP - ('1 MINUTE'::INTERVAL * user_options.digest_after_minutes)")

          query = query.where("approved OR moderator OR admin") if SiteSetting.must_approve_users?

          query.pluck(:id)
        end
      end
    end
  end

  ::UserNotifications.send(:include, CustomDigest)
  ::Jobs::EnqueueDigestEmails.send(:include, CustomDigestJob)
}